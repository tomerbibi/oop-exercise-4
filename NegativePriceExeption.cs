﻿using System;
using System.Runtime.Serialization;

namespace oop___exercise___4
{
    [Serializable]
    internal class NegativePriceExeption : Exception
    {
        public NegativePriceExeption()
        {
        }

        public NegativePriceExeption(string message) : base(message)
        {
        }

        public NegativePriceExeption(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NegativePriceExeption(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}