﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___exercise___4
{
    class OrderBook
    {
        private List<Order> _orders;
        private FoodCatalog _catalog;

        public OrderBook(List<Order> orders, FoodCatalog catalog)
        {
            _orders = orders;
            _catalog = catalog;
        }

        public double TotalPrice
        {
            get
            {
                double total = 0;
                _orders.ForEach(item =>
                {
                    total += item.Total;
                });
                return total;
            }
        }
        // i guess that when you said all the active orders you ment the orders that havent been recived yet
        public int TotalNumOfOrders
        {
            get
            {
                int orders = 0;
                _orders.ForEach(item =>
                {
                    if (item.OrderReciveTime == null)
                        orders++; ;
                });
                return orders;
            }
        }
        public static bool operator +(OrderBook book, Order order)
        {
            book._orders.ForEach(item =>
            {
                item._foodItems.ForEach(foodItem =>
                {
                    if (book._catalog[foodItem.Name] == null) ;// i just needed to use the indexer because the
                    // indexer throw the exeption if the foodItem.Name doesnt exist in the catalog
                });

            });
            book._orders.Add(order);
            return true;
        }
        public static bool operator -(OrderBook book, Order order)
        {
            book._orders.ForEach(item =>
            {
                item._foodItems.ForEach(foodItem =>
                {
                    if (book._catalog[foodItem.Name] == null) ;
                });

            });
            book._orders.Remove(order);
            return true;
        }

        public List<FoodItem> PackOnScooter(double maxVolume)
        {
            double CurrentVolume = 0;
            List<FoodItem> items = new List<FoodItem>();
            _orders.ForEach(item =>
            {
            item._foodItems.ForEach(foodItem =>
            {
                if (foodItem.Volume + CurrentVolume < maxVolume)
                {
                    CurrentVolume += foodItem.Volume;
                    items.Add(foodItem);
                }
            });

            });
            return items;
        }

        public List<Order> GetExpensive(int x)
        {
            List<Order> orders = new List<Order>();
            int ordersInList = 0;
            _orders.ForEach(item =>
            {
                if (ordersInList < x)
                    orders.Add(item);
                if(!orders.Contains(item))
                {
                    orders.ForEach(itemInOrders =>
                    {
                        if (item.Total > itemInOrders.Total)
                        {
                            orders.Add(item);
                            orders.Remove(itemInOrders);
                        }
                    });
                }
            });
            return orders;
        }
        public List<Order> this[string name]
        {
            get
            {
                List<Order> orders = new List<Order>();
                _orders.ForEach(item =>
                {
                    item._foodItems.ForEach(foodItem =>
                    {
                        if (foodItem.Name == name)
                            orders.Add(item);
                    });

                });
                return orders;
            }
        }

    }
}
