﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___exercise___4
{
    public class FoodCatalog
    {
        public List<FoodItem> _foodItems = new List<FoodItem>(); // i had to make it public for the + operator and the - operator

        public FoodCatalog(List<FoodItem> foodItems)
        {
            _foodItems = foodItems;
        }

        // in OrderBook and for PackOnScooter in Order book
        public int Count { get { return _foodItems.Count; } }
        public int CountEat
        {
            get
            {
                int h = 0;
                _foodItems.ForEach(item =>
                {
                    if (item.IsFood)
                        h++;
                });
                return h;
            }
        }
        public int CountDrink
        {
            get
            {
                int h = 0;
                _foodItems.ForEach(item =>
                {
                    if (!item.IsFood)
                        h++;
                });
                return h;
            }
        }
        public FoodItem this[string name]
        {
            get
            {
                FoodItem f = null;
                _foodItems.ForEach(item =>
                {
                    if (name == item.Name)
                        f = item;
                });
                if (f is null)
                    throw new FoodItemNotFoundExeption();
                return f;
            }
            set
            {
                int count = 0;
                _foodItems.ForEach(item =>
                {
                    if (name == item.Name)
                        _foodItems[count] = value;
                    count++;
                });
            }
        }
        public List<double> this[double price]
        {
            get
            {
                List<double> closestItemsToPrice = new List<double>();
                double closestPrice = 0;
                _foodItems.ForEach(item =>
                {
                    if (Math.Abs(price - item.Price) < Math.Abs(closestPrice - item.Price))
                    {
                        closestItemsToPrice.Clear();
                        closestPrice = item.Price;
                        closestItemsToPrice.Add(item.Price);
                    }
                    if (Math.Abs(price - item.Price) == Math.Abs(closestPrice - item.Price))
                        closestItemsToPrice.Add(item.Price);
                });
                return closestItemsToPrice;
            }
        }
        public static List<FoodItem> operator +(FoodCatalog catalog, FoodItem item)
        {
            // you didnt write what should i return so i assume that its the foodItem list 
            catalog._foodItems.Add(item);
            return catalog._foodItems;
        }
        public static List<FoodItem> operator -(FoodCatalog catalog, FoodItem item)
        {
            catalog._foodItems.Remove(item);
            return catalog._foodItems;
        }
        public override string ToString()
        {
            string text = null;
            _foodItems.ForEach(item => text += item.Name + ", ");
            return text;
        }
    }
}
