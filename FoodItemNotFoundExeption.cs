﻿using System;
using System.Runtime.Serialization;

namespace oop___exercise___4
{
    [Serializable]
    internal class FoodItemNotFoundExeption : Exception
    {
        public FoodItemNotFoundExeption()
        {
        }

        public FoodItemNotFoundExeption(string message) : base(message)
        {
        }

        public FoodItemNotFoundExeption(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FoodItemNotFoundExeption(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}