﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___exercise___4
{
    public class FoodItem
    {
        public FoodItem(string name, double price, bool isFood, double volume)
        {
            if (name.Length < 2)
                throw new ArgumentException($"the name of the dish cant be less than 2 letters. the name that was given is {name}");
            if (price == 0)
                throw new ArgumentException($"the price must be more than zero");
            if (price < 0)
                throw new NegativePriceExeption($"the price must be more than zero. the price that was given is {price}");
            if (volume <= 0)
                throw new ArgumentException($"the volume must be more than zero. the volume that was given is {volume}");
            Name = name;
            Price = price;
            IsFood = isFood; // im just doing that if its not food its a drink
            Volume = volume;
        }
        public static bool operator == (FoodItem f1, FoodItem f2)
        {
            if (f1 == null || f2 == null)
                return false;
            if (f1 == null && f2 == null)
                return true;
            return f1.Name.ToUpper() == f2.Name.ToUpper();
        }
        public static bool operator !=(FoodItem f1, FoodItem f2)
        {
            return !(f1 == f2);
        }

        public string Name { get;}
        public double Price { get;}
        public bool IsFood { get;}
        public double Volume { get;}

        public override bool Equals(object obj)
        {
            return this == obj as FoodItem;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return $"name: {Name}, price: {Price}, volume: {Volume}, is it food: {IsFood}";
        }
    }
}
