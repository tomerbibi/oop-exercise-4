﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___exercise___4
{
    class Order
    {
        private readonly DateTime _orderCreateTime;
        public List<FoodItem> _foodItems = new List<FoodItem>();// i had to make it public for the + operator and the - operator
        // in OrderBook and for PackOnScooter in Order book
        public double Total 
        {
            get
            {
                double total = 0;
                _foodItems.ForEach(item => { total += item.Price; });
                return total;
            }
        }
        public DateTime OrderCreateTime { get { return _orderCreateTime; } }
        public DateTime OrderReciveTime { get; set; }
        public string CustomerAdress { get; set; }
        public Order(List<FoodItem> foodItems)
        {
            _orderCreateTime = DateTime.Now;
            _foodItems = foodItems;
        }
        public static bool operator +(Order order, FoodItem foodItem)
        {
            if (foodItem == null)
                return false;
            order._foodItems.Add(foodItem);
            return true;
        }
        public static bool operator -(Order order, FoodItem foodItem)
        {
            if (foodItem == null)
                return false;
            order._foodItems.Remove(foodItem);
            return true;
        }
        public static bool operator >(Order o1, Order o2)
        {
            // the "bigger" order is the one that was ordered earlier so its the smaller in the DayTime operator
            if (o1 is null || o2 is null)
                throw new ArgumentNullException("at least one of the orders is null");
            if (o1.OrderCreateTime < o2.OrderCreateTime)
                return true;
            if (o1.OrderCreateTime > o2.OrderCreateTime)
                return false;
            return false;
        }
        public static bool operator <(Order o1, Order o2)
        {
            if (o1.OrderCreateTime > o2.OrderCreateTime)
                return true;
            if (o1.OrderCreateTime < o2.OrderCreateTime)
                return false;
            return false;
        }

        public override string ToString()
        {
            string text = null;
            _foodItems.ForEach(item => text += item.Name + ", ");
            return text;
        }
    }
}
