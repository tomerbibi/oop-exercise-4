﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___exercise___4
{
    class Program
    {
        static void Main(string[] args)
        {
            FoodItem stake = new FoodItem("stake", 89, true, 8);
            FoodItem burger = new FoodItem("burger", 70, false, 5);
            FoodItem orange = new FoodItem("orange", 96, false, 9);
            FoodItem banana = new FoodItem("banana", 45, false, 4);
            FoodItem fruitSalad = new FoodItem("fruit salad", 95, false, 7);
            FoodItem cola = new FoodItem("cola", 38, false, 7);
            FoodItem limonade = new FoodItem("limonade", 28, false, 3);
            FoodItem beer = new FoodItem("beer", 85, false, 8);
            FoodItem orangeJuice = new FoodItem("orange juice", 69, false, 6);
            FoodItem watermelon = new FoodItem("water melon",73, false, 4);

            List<FoodItem> foodItems = new List<FoodItem>()
            {
                stake,
                burger,
                orange,
                banana,
                fruitSalad,
                cola,
                limonade,
                beer,
                orangeJuice,
                watermelon

            };

            FoodCatalog catalog = new FoodCatalog(foodItems);

            List<FoodItem> o1items = new List<FoodItem>()
            {
                limonade,
                orangeJuice,
                burger
            };
            Order o1 = new Order(o1items);

            List<FoodItem> o2items = new List<FoodItem>()
            {
                banana,
                stake
            };
            Order o2 = new Order(o2items);

            List<FoodItem> o3items = new List<FoodItem>()
            {
                fruitSalad,
                stake
            };
            Order o3 = new Order(o3items);

            List<FoodItem> o4items = new List<FoodItem>()
            {
                beer,
                burger
            };
            Order o4 = new Order(o4items);

            List<FoodItem> o5items = new List<FoodItem>()
            {
                watermelon,
                limonade
            };
            Order o5 = new Order(o5items);

            List<Order> orders = new List<Order>()
            {
                o1,
                o2,
                o3,
                o4,
                o5
            };

            OrderBook book = new OrderBook(orders, catalog);
            book.PackOnScooter(28);
            List<FoodItem> packTest = new List<FoodItem>();
            packTest = book.PackOnScooter(80);
            Console.WriteLine(packTest[0]);
            Console.WriteLine(packTest[3]);


        }
    }
}
